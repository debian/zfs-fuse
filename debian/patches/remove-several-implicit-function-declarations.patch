From f2ce304d99d4bd20593e549a42ba7dfb59512534 Mon Sep 17 00:00:00 2001
From: Arjun Shankar <arjun@redhat.com>
Date: Tue, 14 Feb 2023 14:24:05 +0100
Subject: [PATCH] Remove several implicit function declarations

Avoid implicit declarations of exit, sigrelse, abort and umem_startup.
This ensures that zfs-fuse continues to build when a compiler does not
support implicit function declarations by default (C99).

The change to src/lib/libsolcompat/include/sys/time.h is a hack to work
around a circular dependency between the libsolcompat sys/time.h and
sys/types.h includes that eventually leads to abort being used before it
is defined by stdlib.h.
---
 src/lib/libsolcompat/include/sys/time.h | 2 +-
 src/lib/libumem/configure               | 4 ++--
 src/lib/libumem/malloc.c                | 1 +
 src/lib/libumem/umem_fail.c             | 1 +
 src/lib/libumem/umem_impl.h             | 2 --
 src/lib/libumem/umem_test.c             | 1 +
 src/lib/libumem/umem_test2.c            | 1 +
 7 files changed, 7 insertions(+), 5 deletions(-)

diff --git a/src/lib/libsolcompat/include/sys/time.h b/src/lib/libsolcompat/include/sys/time.h
index aa8a07fe..7d24d97a 100644
--- a/src/lib/libsolcompat/include/sys/time.h
+++ b/src/lib/libsolcompat/include/sys/time.h
@@ -59,7 +59,7 @@ static inline hrtime_t gethrtime(void) {
 		fprintf(stderr, "Error: clock_gettime(CLOCK_MONOTONIC) failed\n");
 		fprintf(stderr, "Make sure you are are running kernel 2.6.x and have glibc 2.3.3 or newer installed\n");
 		fprintf(stderr, "Aborting...\n");
-		abort();
+		__builtin_abort();
 	}
 
 	return (((u_int64_t)ts.tv_sec) * NANOSEC) + ts.tv_nsec;
diff --git a/src/lib/libumem/configure b/src/lib/libumem/configure
index aedccddb..70168754 100755
--- a/src/lib/libumem/configure
+++ b/src/lib/libumem/configure
@@ -4231,8 +4231,8 @@ main ()
   for (i = 0; i < 256; i++)
     if (XOR (islower (i), ISLOWER (i))
 	|| toupper (i) != TOUPPER (i))
-      exit(2);
-  exit (0);
+      return 2;
+  return 0;
 }
 _ACEOF
 rm -f conftest$ac_exeext
diff --git a/src/lib/libumem/malloc.c b/src/lib/libumem/malloc.c
index 7eec2076..24f3058a 100644
--- a/src/lib/libumem/malloc.c
+++ b/src/lib/libumem/malloc.c
@@ -38,6 +38,7 @@
 #endif
 
 #include "umem_base.h"
+#include <umem_impl.h>
 
 #include "misc.h"
 
diff --git a/src/lib/libumem/umem_fail.c b/src/lib/libumem/umem_fail.c
index d38dc7c8..74d36383 100644
--- a/src/lib/libumem/umem_fail.c
+++ b/src/lib/libumem/umem_fail.c
@@ -33,6 +33,7 @@
  * Failure routines for libumem (not standalone)
  */
 
+#define _GNU_SOURCE
 #include "config.h"
 #include <sys/types.h>
 #include <signal.h>
diff --git a/src/lib/libumem/umem_impl.h b/src/lib/libumem/umem_impl.h
index f9a37230..cad7d7c1 100644
--- a/src/lib/libumem/umem_impl.h
+++ b/src/lib/libumem/umem_impl.h
@@ -412,10 +412,8 @@ typedef struct umem_cpu {
 #define	UMEM_READY_INITING		2
 #define	UMEM_READY			3
 
-#ifdef UMEM_STANDALONE
 extern void umem_startup(caddr_t, size_t, size_t, caddr_t, caddr_t);
 extern int umem_add(caddr_t, size_t);
-#endif
 
 #ifdef	__cplusplus
 }
diff --git a/src/lib/libumem/umem_test.c b/src/lib/libumem/umem_test.c
index a90e91fb..e7a7206f 100644
--- a/src/lib/libumem/umem_test.c
+++ b/src/lib/libumem/umem_test.c
@@ -3,6 +3,7 @@
 #include <string.h>
 
 #include "umem.h"
+#include <umem_impl.h>
 
 int main(int argc, char *argv[])
 {
diff --git a/src/lib/libumem/umem_test2.c b/src/lib/libumem/umem_test2.c
index 8994b427..092c779a 100644
--- a/src/lib/libumem/umem_test2.c
+++ b/src/lib/libumem/umem_test2.c
@@ -2,6 +2,7 @@
 #include <string.h>
 
 #include "umem.h"
+#include <umem_impl.h>
 
 static const char *TESTSTRINGS[] = {
   "fred",
